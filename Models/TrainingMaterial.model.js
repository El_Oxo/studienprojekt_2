const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrainingMaterialSchema = new Schema({
  ID: Number,
  name: String,
  url: String,
  topic: String,
  contactPerson: mongoose.Types.ObjectId
}, { versionKey: false });

const TrainingMaterial = mongoose.model('trainingMaterial', TrainingMaterialSchema,'trainingMaterial');
module.exports = TrainingMaterial;
