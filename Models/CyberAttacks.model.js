const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CyberAttacksSchema = new Schema({
  ID: Number,
  name: String,
  risk: Number,
  singlePersonAttack: Boolean,
  type: String,
  description: String,
  contactPerson: mongoose.Types.ObjectId
}, { versionKey: false })


const CyberAttacks = mongoose.model('cyberAttacks', CyberAttacksSchema, 'cyberAttacks')
module.exports = CyberAttacks
