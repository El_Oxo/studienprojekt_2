
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContactPersonSchema = new Schema({
  ID: Number,
  name: String,
  department: String,
  mail: String
}, { versionKey: false });

const ContactPerson = mongoose.model('contactPerson', ContactPersonSchema,'contactPerson');
module.exports = ContactPerson;
