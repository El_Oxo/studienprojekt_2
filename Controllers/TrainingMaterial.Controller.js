const createError = require('http-errors');
const mongoose = require('mongoose');

const TrainingMaterial = require('../Models/TrainingMaterial.model');

module.exports = {
  getAllTrainingMaterial: async (req, res, next) => {
    try {
      const results = await TrainingMaterial.find();
      res.status(200).send(results);
    } catch (error) {
      console.log(error.message);
      next(error);
    }
  },

  createNewTrainingMaterial: async (req, res, next) => {
    try {
      const trainingMaterial = new TrainingMaterial(req.body);
      const result = await trainingMaterial.save();
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === 'ValidationError') {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  },

  findTrainingMaterialById: async (req, res, next) => {
    const id = req.params.id;
    try {
       const trainingMaterial = await TrainingMaterial.findOne({ _id: id });
      if (!trainingMaterial) {
        throw createError(404, 'TrainingMaterial does not exist.');
      }
      res.status(200).send(trainingMaterial);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid TrainingMaterial id'));
        return;
      }
      next(error);
    }
  },

  updateATrainingMaterial: async (req, res, next) => {
    try {
      const id = req.params.id;
      const updates = req.body;
      const options = { new: true };

      const result = await TrainingMaterial.findByIdAndUpdate(id, updates, options);
      if (!result) {
        throw createError(404, 'TrainingMaterial does not exist');
      }
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        return next(createError(400, 'Invalid TrainingMaterial Id'));
      }

      next(error);
    }
  },

  deleteATrainingMaterial: async (req, res, next) => {
    const id = req.params.id;
    try {
      const result = await TrainingMaterial.findByIdAndDelete(id);
      if (!result) {
        throw createError(404, 'TrainingMaterial does not exist.');
      }
      const response = {
        message: "Todo successfully deleted",
        id: req.params.id
      };
      return res.status(200).send(response);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid TrainingMaterial id'));
        return;
      }
      next(error);
    }
  }
};
