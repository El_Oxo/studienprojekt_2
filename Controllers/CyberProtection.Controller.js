const createError = require('http-errors');
const mongoose = require('mongoose');

const CyberProtection = require('../Models/CyberProtection.model');

module.exports = {
  getAllCyberProtection: async (req, res, next) => {
    try {
      const results = await CyberProtection.find();
      res.status(200).send(results);
    } catch (error) {
      console.log(error.message);
      next(error);
    }
  },

  createNewCyberProtection: async (req, res, next) => {
    try {
      const cyberProtection = new CyberProtection(req.body);
      const result = await cyberProtection.save();
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === 'ValidationError') {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  },

  findCyberProtectionById: async (req, res, next) => {
    const id = req.params.id;
    try {
       const cyberProtection = await CyberProtection.findOne({ _id: id });
      if (!cyberProtection) {
        throw createError(404, 'CyberProtection does not exist.');
      }
      res.status(200).send(cyberProtection);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid CyberProtection id'));
        return;
      }
      next(error);
    }
  },

  updateACyberProtection: async (req, res, next) => {
    try {
      const id = req.params.id;
      const updates = req.body;
      const options = { new: true };

      const result = await CyberProtection.findByIdAndUpdate(id, updates, options);
      if (!result) {
        throw createError(404, 'CyberProtection does not exist');
      }
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        return next(createError(400, 'Invalid CyberProtection Id'));
      }

      next(error);
    }
  },

  deleteACyberProtection: async (req, res, next) => {
    const id = req.params.id;
    try {
      const result = await CyberProtection.findByIdAndDelete(id);
      if (!result) {
        throw createError(404, 'CyberProtection does not exist.');
      }
      const response = {
        message: "Todo successfully deleted",
        id: req.params.id
      };
      return res.status(200).send(response);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid CyberProtection id'));
        return;
      }
      next(error);
    }
  }
};
